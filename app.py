from flask import Flask, render_template

from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop

from bokeh.application import Application
from bokeh.application.handlers import FunctionHandler
from bokeh.embed import server_document
from bokeh.layouts import column
from bokeh.models import ColumnDataSource, Slider
from bokeh.plotting import figure
from bokeh.server.server import Server





from bokeh.sampledata.sea_surface_temperature import sea_surface_temperature

application = Flask(__name__)

def modify_doc(doc):
    df = sea_surface_temperature.copy()
    source = ColumnDataSource(data=df)

    plot = figure(x_axis_type='datetime', y_range=(0, 25), y_axis_label='Temperature (Celsius)',
                  title="Sea Surface Temperature at 43.18, -70.43")
    plot.line('time', 'temperature', source=source)

    def callback(attr, old, new):
        if new == 0:
            data = df
        else:
            data = df.rolling('{0}D'.format(new)).mean()
        source.data = ColumnDataSource(data=data).data

    slider = Slider(start=0, end=30, value=0, step=1, title="Smoothing by N Days")
    slider.on_change('value', callback)

    doc.add_root(column(slider, plot))

 

# can't use shortcuts here, since we are passing to low level BokehTornado

# This is so that if this app is run using something like "gunicorn -w 4" then
# each process will listen on its own port


@application.route('/', methods=['GET'])
def bkapp_page():
    script = server_document('http://localhost:5006/bkapp')
    return render_template("embed.html", script=script, template="Flask")

def bk_worker():
    server = Server({'/bkapp': modify_doc}, allow_websocket_origin=["localhost:8080"])
    server.start()
    server.io_loop.start()

from threading import Thread
Thread(target=bk_worker).start()
if __name__ == '__main__':
    application.run(port=8080)